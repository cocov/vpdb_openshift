from django.contrib import admin
from django.utils.html import mark_safe
from django.utils.encoding import force_text
from django.contrib.contenttypes.admin import GenericTabularInline, GenericStackedInline
from generic.models import Comment
from nested_inline.admin import NestedStackedInline, NestedModelAdmin, NestedTabularInline




# Mixin classes for readonly/granular permission features
class VPDBModelAdminPermissionMixin():
    readonly_fields = []
    admin_group = None
    expert_group = None

    def get_readonly_fields(self, request, obj=None):
        if self.user_is_readonly(request):
            readonly_fields = [field.name for field in obj._meta.fields]
            try:
                readonly_fields.remove('id')
            except ValueError:
                pass

            try:
                for field in self.fields:
                    if not field in readonly_fields:
                        readonly_fields += [ field ]
            except Exception:
                try:
                    for group in self.fieldsets:
                        group_dict = group[1]
                        for key, fields in group_dict.items():
                            if key == 'fields':
                                for field in fields:
                                    if not field in readonly_fields:
                                        readonly_fields += [ field ]
                except Exception:
                    pass


        else:
            readonly_fields = []

        return readonly_fields

    def has_change_permission(self, request, obj=None):
        if self.user_is_readonly(request):
            return (request.method in ['GET', 'HEAD'])
        else:
            return super().has_change_permission(request, obj)

    def has_add_permission(self, request):
        if self.user_is_readonly(request):
            return False
        else:
            return super().has_add_permission(request)

    def has_delete_permission(self, request, obj=None):
        if self.user_is_readonly(request):
            return False
        else:
            return super().has_delete_permission(request, obj)

    def change_view(self, request, object_id, form_url='', extra_context=None):
        if not extra_context or 'title' not in extra_context:
            title = force_text(self.model._meta.verbose_name).capitalize()

            try:
                extra_context.update({'title' : title})
            except Exception:
                extra_context = {'title' : title}

        if self.user_is_readonly(request):
            extra_context.update(
               {
                   'show_save' : False,
                   'show_save_and_continue' : False,
                   'show_save_as_new' : False,
                   'show_save_and_add_another' : False,
                   'show_delete_link' : False,
               }
            )

        return super().change_view(request, object_id, form_url=form_url, extra_context=extra_context)

    def get_actions(self, request):
        actions = super().get_actions(request)
        if not self.has_delete_permission(request):
            try:
                del actions['delete_selected']
            except KeyError:
                pass
        return actions

    def user_is_readonly(self, request):
        groups = [ g.name for g in request.user.groups.all() ]
        if request.user == 'vpreader':
            return True
        elif (self.expert_group in groups) or (self.admin_group in groups) or request.user.is_superuser:
            return False
        else:
            return True

    def get_formsets_with_inlines(self, request, obj=None):
        for inline in self.get_inline_instances(request, obj):
            if obj == None:
                continue
            yield inline.get_formset(request, obj), inline


class VPDBModelAdminCommentPermissionMixin(VPDBModelAdminPermissionMixin):

    def save_formset(self, request, form, formset, change):
        instances = formset.save(commit=False)
        comment_error = False
        for obj in formset.deleted_objects:
            if obj.__class__ is Comment:
                if obj.user == request.user:
                    obj.delete()
                else:
                    comment_error = True
                    messages.warning(request, 'You were not allowed to delete a comment posted by {} {}'.format(obj.user.first_name, obj.user.last_name))
            else:
                obj.delete()

        for instance in instances:
            if instance.__class__ is Comment:
                if not instance.pk:
                    instance.user = request.user
                    instance.save()
                elif instance.user != request.user:
                    comment_error = True
                    messages.warning(request, 'You were not allowed to edit a comment posted by {} {}'.format(obj.user.first_name, obj.user.last_name))
            else:
                instance.save()

        if comment_error:
            messages.error(request, 'You are not allowed to delete or edit comments posted by other users. Any other changes have been saved.')
        formset.save_m2m()

class VPDBModelAdminInlinePermissionMixin(VPDBModelAdminPermissionMixin):
    extra = 0

    def link(self, obj):
        if obj and obj.pk:
            url = '/vpdb/{}/{}/{}/change/'.format(obj._meta.app_label, obj.__class__.__name__.lower(), obj.pk)
            response = mark_safe('<a href="{}">full page view</a>'.format(url))
        elif obj:
            url = '/vpdb/{}/{}/add/'.format(obj._meta.app_label, obj.__class__.__name__.lower())
            response = mark_safe('<a href="{}">full page view</a>'.format(url))
        else:
            response = mark_safe('--')

        return response
    link.short_description = 'Details'

    def get_readonly_fields(self, request, obj=None):
        readonly_fields = super().get_readonly_fields(request, obj)

        if obj and obj.pk:
            readonly_fields += ['link']

        return readonly_fields

# Admin models with readonly/granular permission features
class VPDBModelAdmin(VPDBModelAdminPermissionMixin, NestedModelAdmin):
    pass

class VPDBModelAdminWithComments(VPDBModelAdminCommentPermissionMixin, NestedModelAdmin):
    pass

class VPDBStackedInline(VPDBModelAdminInlinePermissionMixin, NestedStackedInline):
    pass

class VPDBTabularInline(VPDBModelAdminInlinePermissionMixin, NestedTabularInline):
    pass

class VPDBGenericStackedInline(VPDBModelAdminInlinePermissionMixin, GenericStackedInline):
    pass

class VPDBGenericTabularInline(VPDBModelAdminInlinePermissionMixin, GenericTabularInline):
    pass
