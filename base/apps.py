from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _
from grappelli.dashboard import modules

class MainConfig(AppConfig):
    name = 'main'

def app_group(name=None, title=None, title_suffix='', direct_models = None): 

    if name == None:
        return None

    if title == None:
        title = name

    group_children = [
	modules.LinkList(
		_(''),
		column=1,
		collapsible=False,
		children=[
			{
				'title': _('{}{}'.format(title, title_suffix)),
				'url': '/vpdb/{}'.format(name),
				'external': False,
			},
                    ]
                ),
    ]

    if None != direct_models and [] != direct_models:
        group_children.append(
    	    modules.ModelList(
    		_('Direct Access'),
    		column=1,
    		collapsible=True,
    		css_classes=('grp-collapse','grp-closed'),
    		models=(direct_models,),
    	    )
        )

    return modules.Group(
	_(title),
	column=1,
	collapsible=True,
	css_classes=('grp-collapse','grp-closed'),
	children = group_children 
    )
