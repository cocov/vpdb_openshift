from django.db import models
from django.contrib.contenttypes.fields import GenericForeignKey,GenericRelation
from django.contrib.contenttypes.models import ContentType


from generic import models as generic_models
########################################################################################################################
# Abstract definitions
########################################################################################################################

class Item(models.Model):
    """
    An abstract class for item
    """
    GRADE_CHOICES = (
        ('A', 'Grade A'),
        ('B', 'Grade B'),
        ('C', 'Grade C'),
        ('D', 'Grade D'),
        ('E', 'Grade E'),
        ('Z', 'Not graded'),
    )
    #: models.CharField : LHCb ID (to be defined in a generate_lhcb_id script)
    lhcb_id = models.CharField(max_length=40, unique=True,null=True,blank=True)
    #: models.CharField : Grade of the item, (to be defined in a generate_grade script)
    grade = models.CharField(max_length=1, choices=GRADE_CHOICES, default='Z', help_text = 'Grade of the item')
    #: models.ForeignKey: Location of the item
    location =  models.ForeignKey(generic_models.Location, on_delete=models.CASCADE,null=True)
    #: GenericRelation: Comments
    comments = GenericRelation(generic_models.Comment)

    class Meta:
        abstract = True

    def generate_lhcb_id(self):
        raise NotImplementedError("generate_lhcb_id have not been specified for this specific item")

    def save(self, *args, **kwargs):
        if not self.lhcb_id :
            try :
                # update the LHCb ID counter
                self.update_lhcbid_counter()
                self.generate_lhcb_id()

            except NotImplementedError:
                print("generate_lhcb_id have not been specified for this specific item")

        super(Item, self).save(*args, **kwargs)

    def __str__(self):
        return self.lhcb_id

    def update_lhcbid_counter(self):
        try :
            print(self._meta.label)
            my_lhcb_id = lhcbID.objects.get(name=self._meta.label)
            my_lhcb_id.cnt =  my_lhcb_id.cnt + 1
            my_lhcb_id.save()

        except Exception as instr:
            id = lhcbID(name = self._meta.label)
            id.save()

class Qualification(models.Model):
    """
    An abstract class for qualification
    """
    GRADE_CHOICES = (
        ('A', 'Grade A'),
        ('B', 'Grade B'),
        ('C', 'Grade C'),
        ('D', 'Grade D'),
        ('E', 'Grade E'),
        ('Z', 'Not graded'),
    )
    #: models.CharField : Name
    name = models.CharField(max_length=80, null=True,blank=True)
    #: models.ForeignKey: Link to the method used
    method = models.ForeignKey(generic_models.Method, on_delete=models.CASCADE)
    #: models.ForeignKey: Link to the operator
    operator = models.ForeignKey(generic_models.Person)
    #: models.DateTimeField: Start date of the qualification
    start_date = models.DateTimeField('Start date', null=True, blank=True)
    #: models.DateTimeField: End date of the qualification
    end_date = models.DateTimeField('End date', null=True, blank=True)
    #: models.CharField : Grade
    grade = models.CharField(max_length=1, choices=GRADE_CHOICES, default='Z')
    #: GenericRelation: Comments
    comments = GenericRelation(generic_models.Comment)

    class Meta:
        abstract = True


    def generate_qualification_name(self):
        raise NotImplementedError("generate_qualification_name have not been specified for this qualification")

    def save(self, *args, **kwargs):
        if not self.name :
            try :
                self.generate_qualification_name()
            except NotImplementedError:
                print("generate_qualification_name have not been specified for this qualification, assign method name")
                self.name = self.method.name

        super(Qualification, self).save(*args, **kwargs)

    def __str__(self):
        return self.method.name


class Process(models.Model):
    """
    An abstract class for qualification
    """
    GRADE_CHOICES = (
        ('A', 'Grade A'),
        ('B', 'Grade B'),
        ('C', 'Grade C'),
        ('D', 'Grade D'),
        ('E', 'Grade E'),
        ('Z', 'Not graded'),
    )
    #: models.CharField : Name
    name = models.CharField(max_length=80, null=True,blank=True)
    #: models.ForeignKey: Link to the method used
    method = models.ForeignKey(generic_models.Method, on_delete=models.CASCADE, null = True)
    #: models.ForeignKey: Link to the operator
    operator = models.ForeignKey(generic_models.Person)
    #: models.DateTimeField: Start date of the qualification
    start_date = models.DateTimeField('Start date', null=True, blank = True)
    #: models.DateTimeField: End date of the qualification
    end_date = models.DateTimeField('End date', null=True, blank = True)
    #: models.CharField : Grade
    grade = models.CharField(max_length=1, choices=GRADE_CHOICES, default='Z')
    #: GenericRelation: Comments
    comments = GenericRelation(generic_models.Comment)

    class Meta:
        abstract = True

    def generate_process_name(self):
        raise NotImplementedError("generate_process_name have not been specified for this process")

    def save(self, *args, **kwargs):
        if not self.name :
            try :
                self.generate_process_name()
            except NotImplementedError:
                print("generate_process_name have not been specified for this process, assign method name")
                self.name = self.method.name

        super(Process, self).save(*args, **kwargs)

    def __str__(self):
        return self.name

class Manufacturer(models.Model):
    """
    An abstract class for item manufacturers
    """
    #: models.CharField : Name of the manufacturer
    name = models.CharField(max_length=40)
    #: models.EmailField : Email of the manufacturer
    email = models.EmailField(blank=True)
    #: models.CharField : Address of the manufacturer
    address = models.CharField(max_length=100, blank=True)

    class Meta:
        abstract = True


class lhcbID(models.Model):

    """
    A class for lhcbid counters to provide unique identifiers
    """

    #:models.CharField : Name of the item
    name = models.CharField(max_length=100)
    #:models.IntegerField : Counter
    cnt = models.IntegerField(default=0)

########################################################################################################################
# Generic models
########################################################################################################################
