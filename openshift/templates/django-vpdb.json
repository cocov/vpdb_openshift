{
  "kind": "Template",
  "apiVersion": "v1",
  "metadata": {
    "name": "django-vpdb",
    "annotations": {
      "openshift.io/display-name": "Django + PostgreSQL",
      "description": "An example Django application with a PostgreSQL database. For more information about using this template, including OpenShift considerations, see https://github.com/openshift/django-ex/blob/master/README.md.",
      "tags": "quickstart,python,django",
      "iconClass": "icon-python",
      "openshift.io/long-description": "This template defines resources needed to develop a Django based application, including a build configuration, application deployment configuration, and database deployment configuration.",
      "openshift.io/provider-display-name": "Red Hat, Inc.",
      "openshift.io/documentation-url": "https://gitlab.cern.ch/cocov/vpdb_openshift.git",
      "openshift.io/support-url": "https://access.redhat.com",
      "template.openshift.io/bindable": "false"
    }
  },
  "message": "The following service(s) have been created in your project: ${NAME}.\n\nFor more information about using this template, including OpenShift considerations, see https://github.com/openshift/django-ex/blob/master/README.md.",
  "labels": {
      "template": "django-vpdb",
      "app": "django-vpdb"
  },
  "objects": [
    {
      "kind": "Secret",
      "apiVersion": "v1",
      "metadata": {
        "name": "${NAME}"
      },
      "stringData" : {
        "database-user" : "${DATABASE_USER}",
        "database-password" : "${DATABASE_PASSWORD}",
        "django-secret-key" : "${DJANGO_SECRET_KEY}",
        "database-host" : "${DATABASE_HOST}",
        "database-port" : "${DATABASE_PORT}"
      }
    },
    {
      "kind": "Service",
      "apiVersion": "v1",
      "metadata": {
        "name": "${NAME}",
        "annotations": {
          "description": "Exposes and load balances the application pods"
        }
      },
      "spec": {
        "ports": [
          {
            "name": "web",
            "port": 8080,
            "targetPort": 8080
          }
        ],
        "selector": {
          "name": "${NAME}"
        }
      }
    },
    {
      "kind": "Route",
      "apiVersion": "v1",
      "metadata": {
        "name": "${NAME}"
      },
      "spec": {
        "host": "${APPLICATION_DOMAIN}",
        "to": {
          "kind": "Service",
          "name": "${NAME}"
        }
      }
    },
    {
      "kind": "ImageStream",
      "apiVersion": "v1",
      "metadata": {
        "name": "${NAME}",
        "annotations": {
          "description": "Keeps track of changes in the application image"
        }
      }
    },
    {
      "kind": "BuildConfig",
      "apiVersion": "v1",
      "metadata": {
        "name": "${NAME}",
        "annotations": {
          "description": "Defines how to build the application",
          "template.alpha.openshift.io/wait-for-ready": "true"
        }
      },
      "spec": {
        "source": {
          "type": "Git",
          "git": {
            "uri": "${SOURCE_REPOSITORY_URL}",
            "ref": "${SOURCE_REPOSITORY_REF}"
          },
          "contextDir": "${CONTEXT_DIR}"
        },
        "strategy": {
          "type": "Source",
          "sourceStrategy": {
            "from": {
              "kind": "ImageStreamTag",
              "namespace": "${NAMESPACE}",
              "name": "python:3.5"
            },
            "env": [
              {
                  "name": "PIP_INDEX_URL",
                  "value": "${PIP_INDEX_URL}"
              }
            ]
          }
        },
        "output": {
          "to": {
            "kind": "ImageStreamTag",
            "name": "${NAME}:latest"
          }
        },
        "triggers": [
          {
            "type": "ImageChange"
          },
          {
            "type": "ConfigChange"
          },
          {
            "type": "GitHub",
            "github": {
              "secret": "${GITHUB_WEBHOOK_SECRET}"
            }
          }
        ],
        "postCommit": {
           "script": "./manage.py test"
        }
      }
    },
    {
      "kind": "DeploymentConfig",
      "apiVersion": "v1",
      "metadata": {
        "name": "${NAME}",
        "annotations": {
          "description": "Defines how to deploy the application server",
          "template.alpha.openshift.io/wait-for-ready": "true"
        }
      },
      "spec": {
        "strategy": {
          "type": "Recreate"
        },
        "triggers": [
          {
            "type": "ImageChange",
            "imageChangeParams": {
              "automatic": true,
              "containerNames": [
                "django-vpdb"
              ],
              "from": {
                "kind": "ImageStreamTag",
                "name": "${NAME}:latest"
              }
            }
          },
          {
            "type": "ConfigChange"
          }
        ],
        "replicas": 1,
        "selector": {
          "name": "${NAME}"
        },
        "template": {
          "metadata": {
            "name": "${NAME}",
            "labels": {
              "name": "${NAME}"
            }
          },
          "spec": {
            "containers": [
              {
                "name": "django-vpdb",
                "image": " ",
                "ports": [
                  {
                    "containerPort": 8080
                  }
                ],
                "readinessProbe": {
                  "timeoutSeconds": 3,
                  "initialDelaySeconds": 3,
                  "httpGet": {
                    "path": "/vpdb",
                    "port": 8080
                  }
                },
                "livenessProbe": {
                  "timeoutSeconds": 3,
                  "initialDelaySeconds": 30,
                  "httpGet": {
                    "path": "/vpdb",
                    "port": 8080
                  }
                },
                "env": [
                  {
                    "name": "DATABASE_NAME",
                    "value": "${DATABASE_NAME}"
                  },
                  {
                    "name": "DATABASE_USER",
                    "valueFrom": {
                      "secretKeyRef" : {
                        "name" : "${NAME}",
                        "key" : "database-user"
                      }
                    }
                  },
                  {
                    "name": "DATABASE_PASSWORD",
                    "valueFrom": {
                      "secretKeyRef" : {
                        "name" : "${NAME}",
                        "key" : "database-password"
                      }
                    }
                  },
                  {
                    "name": "APP_CONFIG",
                    "value": "${APP_CONFIG}"
                  },
                  {
                    "name": "DJANGO_SECRET_KEY",
                    "valueFrom": {
                      "secretKeyRef" : {
                        "name" : "${NAME}",
                        "key" : "django-secret-key"
                      }
                    }
                  }
                ],
                "resources": {
                  "limits": {
                    "memory": "${MEMORY_LIMIT}"
                  }
                }
              }
            ]
          }
        }
      }
    }
  ],
  "parameters": [
    {
      "name": "NAME",
      "displayName": "Name",
      "description": "The name assigned to all of the frontend objects defined in this template.",
      "required": true,
      "value": "django-vpdb"
    },
    {
      "name": "NAMESPACE",
      "displayName": "Namespace",
      "required": true,
      "description": "The OpenShift Namespace where the ImageStream resides.",
      "value": "openshift"
    },
    {
      "name": "MEMORY_LIMIT",
      "displayName": "Memory Limit",
      "required": true,
      "description": "Maximum amount of memory the Django container can use.",
      "value": "512Mi"
    },
    {
      "name": "MEMORY_POSTGRESQL_LIMIT",
      "displayName": "Memory Limit (PostgreSQL)",
      "required": true,
      "description": "Maximum amount of memory the PostgreSQL container can use.",
      "value": "512Mi"
    },
    {
      "name": "VOLUME_CAPACITY",
      "displayName": "Volume Capacity",
      "description": "Volume space available for data, e.g. 512Mi, 2Gi",
      "value": "1Gi",
      "required": true
    },
    {
      "name": "SOURCE_REPOSITORY_URL",
      "displayName": "Git Repository URL",
      "required": true,
      "description": "The URL of the repository with your application source code.",
      "value": "https://gitlab.cern.ch/cocov/vpdb_openshift.git"
    },
    {
      "name": "SOURCE_REPOSITORY_REF",
      "displayName": "Git Reference",
      "description": "Set this to a branch name, tag or other ref of your repository if you are not using the default branch."
    },
    {
      "name": "CONTEXT_DIR",
      "displayName": "Context Directory",
      "description": "Set this to the relative path to your project if it is not in the root of your repository."
    },
    {
      "name": "APPLICATION_DOMAIN",
      "displayName": "Application Hostname",
      "description": "The exposed hostname that will route to the Django service, if left blank a value will be defaulted.",
      "value": ""
    },
    {
      "name": "GITHUB_WEBHOOK_SECRET",
      "displayName": "GitHub Webhook Secret",
      "description": "Github trigger secret.  A difficult to guess string encoded as part of the webhook URL.  Not encrypted.",
      "generate": "expression",
      "from": "[a-zA-Z0-9]{40}"
    },
    {
      "name": "DATABASE_NAME",
      "displayName": "Database Name",
      "required": true,
      "value": "default"
    },
    {
      "name": "DATABASE_USER",
      "displayName": "Database Username",
      "required": true,
      "value": "django"
    },
    {
      "name": "DATABASE_PASSWORD",
      "displayName": "Database User Password",
      "generate": "expression",
      "from": "[a-zA-Z0-9]{16}"
    },
    {
      "name": "DATABASE_HOST",
      "displayName": "Database Host",
      "description": "Host of the postgresql database"
      "required": true
    },
    {
      "name": "DATABASE_PORT",
      "displayName": "Database Port",
      "description": "Port to access the postgresql database on the host"
      "required": true
    },
    {
      "name": "APP_CONFIG",
      "displayName": "Application Configuration File Path",
      "description": "Relative path to Gunicorn configuration file (optional)."
    },
    {
      "name": "DJANGO_SECRET_KEY",
      "displayName": "Django Secret Key",
      "description": "Set this to a long random string.",
      "generate": "expression",
      "from": "[\\w]{50}"
    },
    {
      "name": "PIP_INDEX_URL",
      "displayName": "Custom PyPi Index URL",
      "description": "The custom PyPi index URL",
      "value": ""
    }
  ]
}
