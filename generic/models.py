from django.db import models
from django.utils.html import mark_safe
from django.contrib.auth.models import User
from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType


class Institute(models.Model):
    """
    A class for the sites
    """
    #: models.CharField : Name of the institute/company

    class Meta:
        db_table = 'generic_institutes'


    #: models.CharField : Name of the institute
    name = models.CharField(max_length=20,help_text = "Name of the institute/company")

    def __str__(self):
        """
        Print the details of this object
        :return: description string
        """
        return '%s'%(self.name)


class Method(models.Model):
    """
    A class for the methods
    """

    class Meta:
        db_table = 'generic_methods'

    TYPE_CHOICES = (('PROCESS','The method is a process'),('QUALIFICATION','The method is a qualification'))
    #: models.CharField : Name of the method
    name = models.CharField(max_length=100,help_text = "Name of the method")
    #: models.CharField : Type of the method
    type = models.CharField(max_length=20, help_text = "Process or qualification",choices=TYPE_CHOICES)
    #: models.TextField: Description of the method
    description = models.TextField(help_text = "Description of the method")
    #: models.CharField: Which item / processes this method applies too (app:object, comma separated, )
    apply_to = models.CharField(max_length=100,
                                help_text = "Which item / processes this method applies too "
                                            "(list of app:object, comma separated)",  blank = True)

    def __str__(self):
        return self.name



class Person(models.Model):
    """
    A class for persons
    """
    class Meta:
        db_table = 'generic_persons'

    #: models.CharField : Name of the person
    last_name = models.CharField(max_length=20,help_text = "Last Name")
    #: models.CharField : First Name of the method
    first_name = models.CharField(max_length=20,help_text = "First Name")
    #: models.EmailField : Email of the person
    email = models.EmailField(blank = True)
    #: models.ForeignKey: Link to the institute/company
    institute = models.ForeignKey(Institute, on_delete=models.CASCADE,help_text = "Institute/Company")

    def __str__(self):
        return '%s %s, %s'%(self.first_name,self.last_name,self.institute.name)


class Comment(models.Model):
    """
    A class for comments
    """
    class Meta:
        db_table = 'generic_comments'

    date = models.DateTimeField('date', auto_now_add=True, editable=False)
    user = models.ForeignKey(User, editable=False, on_delete=models.CASCADE)
    subject = models.CharField(max_length=128)
    text = models.TextField()    
    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE)
    object_id = models.PositiveIntegerField()
    commented_object = GenericForeignKey('content_type', 'object_id')

    def __str__(self):
        timestamp = self.date.replace(microsecond=0, tzinfo=None)
        display_string = mark_safe('"{}", {} {}, {}'.format(self.subject, self.user.first_name, self.user.last_name, timestamp))

        return display_string

class Location(models.Model):
    """
    A class for item locations
    """
    class Meta:
        db_table = 'generic_locations'

    #: models.ForeignKey: Link to the institute/company's location
    institute = models.ForeignKey(Institute, on_delete=models.CASCADE,help_text = "Institute/Company were the item is located")
    #: models.CharField : Name of the room, including building (optional)
    room = models.CharField(max_length=100, blank=True,help_text = "Room location: Blg,Lab,etc... (optional)")
    #: models.CharField : Specfic location in the room (optional)
    shelve = models.CharField(max_length=100, blank=True,help_text = "Where in the room? shelve,bench, etc... (optional)")

    def __str__(self):
        """
        Print the details of this object
        :return: description string
        """
        display_string = '{}'.format(self.institute)
        if self.room:
            display_string += ', {}'.format(self.room)
        if self.shelve:
            display_string += ', {}'.format(self.shelve)
        return display_string

