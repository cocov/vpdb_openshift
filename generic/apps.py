from django.apps import AppConfig
import base.apps

name = 'generic'
verbose_name = 'Common Items'

class GenericConfig(AppConfig):
    name = name
    verbose_name = verbose_name

def index_group(): 
    return base.apps.app_group(name=name,
        title=verbose_name,
        #title_suffix=None,
        direct_models='generic.models.*')

