from vpdb.admin import vpdbsite as site

from base.admin import *
from .models import *

# groups configuration mixin
class GenericAppGroups:
    admin_group = 'generic_admin'
    expert_group = 'generic_expert'

# Admin models for this app
class GenericAppModelAdmin(GenericAppGroups, VPDBModelAdminWithComments):
    pass

class GenericAppStackedInline(GenericAppGroups, VPDBStackedInline):
    pass

class GenericAppTabularInline(GenericAppGroups, VPDBTabularInline):
    pass

class GenericAppGenericStackedInline(GenericAppGroups, VPDBGenericStackedInline):
    pass

class GenericAppGenericTabularInline(GenericAppGroups, VPDBGenericTabularInline):
    pass

class LocationTabularInline(GenericAppTabularInline):
    model = Location
    fields = ( 'link', 'room', 'shelve', )
    fk_name = 'institute'

class CommentStackedInline(GenericAppGenericStackedInline):
    model = Comment
    verbose_name = 'comment'
    verbose_name_plural = 'comments'
    classes = ('grp-collapse grp-open',)
    inline_classes = ('grp-collapse grp-closed',)
    fieldsets = (
            ( None, { 
                'fields' : ( 'subject', 'text', ), 
            } ),
    )

class CommentModelAdmin(GenericAppModelAdmin):
    fieldsets = (
            ( None, { 'fields' : ( 'subject', 'display_user', 'date',  ) } ),
            ( None, { 'fields' : ( 'text', ) } ),
        )

    def get_model_perms(self, request):
        return {}

    def get_readonly_fields(self, request, obj=None):
        readonly_fields = super().get_readonly_fields(request, obj)
        readonly_fields += ['date', 'display_user']
        
        if request.user.is_superuser:
            return readonly_fields

        try:
            if obj.user != request.user:
                if 'subject' not in readonly_fields:
                    readonly_fields += ['subject']
                if 'text' not in readonly_fields:
                    readonly_fields += ['text']
        except Exception:
            pass

        return readonly_fields

class LocationModelAdmin(GenericAppModelAdmin):
    inlines = ( CommentStackedInline, )


class InstituteModelAdmin(GenericAppModelAdmin):
    inlines = ( LocationTabularInline, CommentStackedInline, )


site.register(Location, LocationModelAdmin )
site.register(Institute, InstituteModelAdmin)
site.register(Person, GenericAppModelAdmin)
site.register(Method, GenericAppModelAdmin)
site.register(Comment,CommentModelAdmin)
