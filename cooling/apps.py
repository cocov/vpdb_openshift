from django.apps import AppConfig
import base.apps

name = 'cooling'
verbose_name = 'Cooling Substrates'

class CoolingConfig(AppConfig):
    name = name
    verbose_name = verbose_name

def index_group(): 
    return base.apps.app_group(name=name,
        title=verbose_name,
        title_suffix=' Project',
        direct_models='cooling.models.*')
