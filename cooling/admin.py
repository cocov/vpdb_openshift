from vpdb.admin import vpdbsite as site

from base.admin import *
from .models import *

class CoolingGroups:
    expert_group = 'cooling_expert'
    admin_group = 'cooling_admin'

class CoolingModelAdmin(CoolingGroups, VPDBModelAdmin):
    pass

class CoolingStackedInline(CoolingGroups, VPDBStackedInline):
    pass

class CoolingTabularInline(CoolingGroups, VPDBTabularInline):
    pass







class MetalliseModelAdmin(CoolingModelAdmin):
    related_lookup_fields = {
        'generic': [['connector_assembly_type', 'object_id']]}

class RemoveMetallisationModelAdmin(CoolingModelAdmin):
    related_lookup_fields = {
        'generic': [['connector_assembly_type', 'object_id']]}

class PlanarityModelAdmin(CoolingModelAdmin):
    related_lookup_fields = {
        'generic': [['content_type', 'object_id']]}

class OperatorReportModelAdmin(CoolingModelAdmin):
    related_lookup_fields = {
        'generic': [['content_type', 'object_id']]}

class VisualInspectionModelAdmin(CoolingModelAdmin):
    related_lookup_fields = {
        'generic': [['content_type', 'object_id']]}

class PressureTestModelAdmin(CoolingModelAdmin):
    related_lookup_fields = {
        'generic': [['content_type', 'object_id']]}

class CleanModelAdmin(CoolingModelAdmin):
    related_lookup_fields = {
        'generic': [['content_type', 'object_id']]}


class MeltSolderingFoilModelAdmin(CoolingModelAdmin):
    related_lookup_fields = {
        'generic': [['melt_on_object_type', 'object_id']]}


class PlateTabularInline(CoolingTabularInline):
    model = Plate
    fields = ( 'link','lhcb_id', 'location')
    #extra = 1
    fk_name = 'from_process'

    def get_readonly_fields(self, request, obj=None):
        readonly_fields = super().get_readonly_fields(request, obj)
        readonly_fields+=['lhcb_id']
        return readonly_fields


class PressureQCTabularInline(CoolingTabularInline):
    model = PressureQC
    fields = ( 'link','lhcb_id', 'location')
    #extra = 1
    fk_name = 'from_process'

    def get_readonly_fields(self, request, obj=None):
        readonly_fields = super().get_readonly_fields(request, obj)
        readonly_fields+=['lhcb_id']
        return readonly_fields


class SolderingQCTabularInline(CoolingTabularInline):
    model = SolderingQC
    fields = ( 'link','lhcb_id', 'location')
    fk_name = 'from_process'

    def get_readonly_fields(self, request, obj=None):
        readonly_fields = super().get_readonly_fields(request, obj)
        readonly_fields+=['lhcb_id']
        return readonly_fields

class AcousticMicroscopeTabularInline(CoolingTabularInline):
    model = AcousticMicroscope
    fields = ( 'link','name', 'microscope_result')
    fk_name = 'item_under_qualification'

    def get_readonly_fields(self, request, obj=None):
        readonly_fields = super().get_readonly_fields(request, obj)
        readonly_fields+=['name']
        return readonly_fields


class PlanarityTabularInline(CoolingTabularInline):
    model = Planarity
    fields = ( 'link','name', 'spread')
    fk_name = 'item_under_qualification'

    def get_readonly_fields(self, request, obj=None):
        readonly_fields = super().get_readonly_fields(request, obj)
        readonly_fields+=['name']
        return readonly_fields


class DiceWaferTabularInline(CoolingTabularInline):
    model = DiceWafer
    fields = ( 'link',)
    fk_name = 'wafer'
    inlines = [PlateTabularInline,PressureQCTabularInline,SolderingQCTabularInline]
    def get_readonly_fields(self, request, obj=None):
        readonly_fields = super().get_readonly_fields(request, obj)
        return readonly_fields


class WaferModelAdmin(CoolingModelAdmin):
    inlines = (DiceWaferTabularInline, AcousticMicroscopeTabularInline,)


class PlateModelAdmin(CoolingModelAdmin):
    inlines = (PlanarityTabularInline,)

class DiceWaferModelAdmin(CoolingModelAdmin):
    inlines = (PlateTabularInline,PressureQCTabularInline,SolderingQCTabularInline,)

#class PressureTestTabularInline(CoolingTabularInline):
    #model = PressureTest
    #fields = ( 'link','name', 'maximal_pressure')
    ##extra = 1
    #fk_name = 'content_type'
#
    #def get_readonly_fields(self, request, obj=None):
        #readonly_fields = super().get_readonly_fields(request, obj)
        #readonly_fields+=['lhcb_id']
        #return readonly_fields

#class PressureQCModelAdmin(CoolingModelAdmin):
#    inlines = (PressureTestTabularInline,)


#class ConnectorAssemblyModelAdmin(CoolingModelAdmin):
#    inlines = (PressureTestTabularInline,)



site.register(CoolingManufacturer, CoolingModelAdmin)

site.register(Wafer, WaferModelAdmin)
site.register(DiceWafer, DiceWaferModelAdmin)
site.register(Plate, CoolingModelAdmin)
site.register(PressureQC, CoolingModelAdmin)
site.register(SolderingQC, CoolingModelAdmin)
site.register(AcousticMicroscope, CoolingModelAdmin)
site.register(PressureTest, CoolingModelAdmin)

site.register(Connector, CoolingModelAdmin)
site.register(Capillary, CoolingModelAdmin)
site.register(VCR, CoolingModelAdmin)
site.register(ConnectorAssembly,CoolingModelAdmin )
site.register(BrazeConnector, CoolingModelAdmin)
site.register(BendCapillary, CoolingModelAdmin)
site.register(Metallise, MetalliseModelAdmin)
site.register(RemoveMetallisation, RemoveMetallisationModelAdmin)

site.register(Clean, CleanModelAdmin)
site.register(MeltSolderingFoil, MeltSolderingFoilModelAdmin)


site.register(Planarity, PlanarityModelAdmin )
site.register(OperatorReport, OperatorReportModelAdmin )
site.register(VisualInspection, VisualInspectionModelAdmin )


