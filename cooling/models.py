from django.db import models
from base import models as base_models
from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType


class CoolingManufacturer(base_models.Manufacturer):
    """
    Manufacturer linked to the cooling project
    """
    class Meta:
        db_table = 'cooling_manufacturers'

    def __str__(self):
        return '%s %s %s' % (self.name, self.email, self.address)



class Wafer(base_models.Item):
    """
    Silicon wafer for cooling
    """

    class Meta:
        db_table = 'cooling_wafer_items'

    #: models.ForeignKey : Link to the manufacturer
    manufacturer = models.ForeignKey(CoolingManufacturer)
    #:str: Wafer ID
    wafer_id = models.CharField(max_length=10,help_text='Wafer ID')
    #: str: batch ID
    batch_id = models.CharField(max_length=10,help_text='batch ID')
    #: models.URLField : EDMS link to the design
    design_edms = models.URLField(blank=True)

    def generate_lhcb_id(self):
        """
        Assign a lhcb_id to the Cooling wafer class following COO_WAF_batchid_waferid
        :return:
        """
        unique_id = base_models.lhcbID.objects.get(name = self._meta.label).cnt
        self.lhcb_id = 'COO_WAF_%d_%s_%s' % (unique_id,self.batch_id, self.wafer_id)

    def save(self, *args, **kwargs):
        """
        Save the SensorWafer object:
        - Fill the EDMS document
        - Generate the sensors if it was required (self.automatic_sensor_creation)

        :param args:
        :param kwargs:
        :return:
        """

        # Assign the EDMS url as a function and generate the sensors
        self.design_edms = ''

        super(Wafer, self).save(*args, **kwargs)

    def __str__(self):
        """
        Print the details of this object
        :return: description string
        """
        return 'Cooling wafer #%s |  batch %s, wafer %s, grade %s, located in %s' % \
               (self.lhcb_id, self.batch_id, self.wafer_id, self.grade, self.location)


class DiceWafer(base_models.Process):
    """
    Dicing of the cooling wafers
    """

    class Meta:
        db_table = 'cooling_dice_wafer_processes'

    wafer = models.ForeignKey(Wafer, on_delete=models.CASCADE)
    #: models.BooleanField
    automatic_plate_creation = models.BooleanField(default=True)

    def save(self, *args, **kwargs):
        """
        Update the diced attribute of the wafer

        :param args:
        :param kwargs:
        :return:
        """

        super(DiceWafer, self).save(*args, **kwargs)

        if self.automatic_plate_creation:
            # First create the cooling plates
            for position in Plate.POSITION_CHOICES:
                if position[0] == 'UNKNOWN':continue
                self.plate_set.create(position=position[0], location=self.wafer.location)
            # Then create the pressure QC
            for position in PressureQC.POSITION_CHOICES:
                if position[0] == 'UNKNOWN':continue
                self.pressureqc_set.create(position=position[0], location=self.wafer.location)
            # Then create the soldering QC
            for position in SolderingQC.POSITION_CHOICES:
                if position[0] == 'UNKNOWN':continue
                self.solderingqc_set.create(position=position[0], location=self.wafer.location)

        self.automatic_plate_creation = False

        super(DiceWafer, self).save(*args, **kwargs)

    def generate_process_name(self):
        self.name = '%s on %s'%(self.method.name,self.wafer.lhcb_id)


class Plate(base_models.Item):
    """
    Cooling plate
    """
    class Meta:
        db_table = 'cooling_plate_items'

    def generate_lhcb_id(self):
        """
        Assign a lhcb_id to the Cooling wafer class following COO_PLT_Wbatchid_waferid
        :return:
        """
        unique_id = base_models.lhcbID.objects.get(name = self._meta.label).cnt
        self.lhcb_id = 'COO_PLT_%d_%s_W%s-%s' % (unique_id,self.position,self.from_process.wafer.batch_id, self.from_process.wafer.wafer_id)


    POSITION_CHOICES = (
        ('TOP','Located on the top of the cooling wafer'),
        ('BOTTOM','Located on the bottom of the cooling wafer'),
        ('UNKNOWN','Not graded'),
    )
    #: str: Position in the wafer (from POSITION_CHOICES)
    position = models.CharField(max_length=7,choices=POSITION_CHOICES,default='UNKOWN',help_text='Position in the wafer')
    #: models.ForeignKey : Link to the wafer dicing process
    from_process =  models.ForeignKey(DiceWafer, on_delete=models.CASCADE)

    def __str__(self):
        """
        Print the details of this object
        :return: description string
        """
        return 'Cooling plate %s | position %s, from wafer %s, grade %s, located in %s' % \
               (self.lhcb_id, self.position, self.from_process.wafer.lhcb_id, self.grade, self.location)


class PressureQC(base_models.Item):
    """
    Part of the wafer used for pressure quality control
    """

    class Meta:
        db_table = 'cooling_pressure_qc_items'

    def generate_lhcb_id(self):
        """
        Assign a lhcb_id to the Cooling wafer class following COO_PLT_batchid_waferid
        :return:
        """
        unique_id = base_models.lhcbID.objects.get(name = self._meta.label).cnt
        self.lhcb_id = 'COO_PQC_%d_%s_W%s-%s' % (unique_id,self.position,self.from_process.wafer.batch_id, self.from_process.wafer.wafer_id)

    POSITION_CHOICES =tuple([('P%d'%i,'P%d'%i) for i in range(24)]) +(
        ('UNKNOWN','Not defined'),
    )
    #: str: Position in the wafer (from POSITION_CHOICES)
    position = models.CharField(max_length=7,choices=POSITION_CHOICES,default='UNKOWN',help_text='Position in the wafer')
    #: models.ForeignKey : Link to the wafer dicing process
    from_process =  models.ForeignKey(DiceWafer, on_delete=models.CASCADE)

    def __str__(self):
        """
        Print the details of this object
        :return: description string
        """
        return 'Pressure QC #%s | position %s, from wafer %s, grade %s, located in %s' % \
               (self.lhcb_id, self.position,self.from_process.wafer.lhcb_id,self.grade,self.location)

class SolderingQC(base_models.Item):
    """
    Part of the wafer used for soldering quality control
    """

    class Meta:
        db_table = 'cooling_soldering_qc_items'

    POSITION_CHOICES =tuple([('S%d'%i,'S%d'%i) for i in range(4)]) +(
        ('UNKNOWN','Not defined'),
    )


    def generate_lhcb_id(self):
        """
        Assign a lhcb_id to the Cooling wafer class following COO_PLT_batchid_waferid
        :return:
        """
        unique_id = base_models.lhcbID.objects.get(name = self._meta.label).cnt
        self.lhcb_id = 'COO_SQC_%d_%s_W%s-%s' % (unique_id,self.position,self.from_process.wafer.batch_id, self.from_process.wafer.wafer_id)

    #: str: Position in the wafer (from POSITION_CHOICES)
    position = models.CharField(max_length=7,choices=POSITION_CHOICES,default='UNKOWN',help_text='Position in the wafer')
    #: models.ForeignKey : Link to the wafer dicing process
    from_process =  models.ForeignKey(DiceWafer, on_delete=models.CASCADE)

    def __str__(self):
        """
        Print the details of this object
        :return: description string
        """
        return 'Soldering QC %s | position %s, from wafer %s, grade %s, located in %s' % \
               (self.lhcb_id, self.position,self.from_process.wafer.lhcb_id,self.grade,self.location)


class Connector(base_models.Item):
    """
    Connector for the CO2 supply to the cooling plate
    """
    class Meta:
        db_table = 'cooling_connector_items'

    def generate_lhcb_id(self):
        """
        Assign a lhcb_id to the Cooling wafer class following COO_PLT_manufacturer_batchid_waferid
        :return:
        """
        unique_id = base_models.lhcbID.objects.get(name = self._meta.label).cnt
        self.lhcb_id = 'COO_CON_%d_%s_%s' % (unique_id,self.prod_id, self.batch_id)

    #: models.ForeignKey : Link to  to the manufacturer
    manufacturer = models.ForeignKey(CoolingManufacturer)
    #: models.CharField Manufacturer defined identification number
    prod_id = models.CharField(max_length=10)
    #: models.CharField Manufacturer defined batch number
    batch_id = models.CharField(max_length=10)

    def __str__(self):
        """
        Print the details of this object
        :return: description string
        """
        return 'Cooling Connector %s | manufacturer %s, prod id %s, grade %s, located in %s' % \
               (self.lhcb_id, self.manufacturer.name,self.prod_id,self.grade,self.location)



class Capillary(base_models.Item):
    """
    Capillaries to supply the cooling plate with CO2
    """
    class Meta:
        db_table = 'cooling_capillaries_items'

    #: models.ForeignKey : Link to  to the manufacturer
    manufacturer = models.ForeignKey(CoolingManufacturer)
    #: models.CharField Manufacturer defined identification number
    prod_id = models.CharField(max_length=10)
    #: models.CharField Manufacturer defined batch number
    batch_id = models.CharField(max_length=10)

    def generate_lhcb_id(self):
        """
        Assign a lhcb_id to the Cooling wafer class following COO_CAP_manufacturer_batchid_waferid
        :return:
        """
        unique_id = base_models.lhcbID.objects.get(name = self._meta.label).cnt
        self.lhcb_id = 'COO_CAP_%d_%s_%s' % (unique_id,self.prod_id, self.batch_id)

    def __str__(self):
        """
        Print the details of this object
        :return: description string
        """
        return 'Cooling Connector %s | manufacturer %s, prod id %s, grade %s, located in %s' % \
               (self.lhcb_id, self.manufacturer.name,self.prod_id,self.grade,self.location)



class VCR(base_models.Item):
    """
    VCR for the capillaries
    """
    class Meta:
        db_table = 'cooling_vcrs_items'

    #: models.ForeignKey : Link to  to the manufacturer
    manufacturer = models.ForeignKey(CoolingManufacturer)
    #: models.CharField Manufacturer defined identification number
    prod_id = models.CharField(max_length=10)
    #: models.CharField Manufacturer defined batch number
    batch_id = models.CharField(max_length=10)
    #: models.CharField Type of VCRs
    type = models.CharField(max_length=10)


    def generate_lhcb_id(self):
        """
        Assign a lhcb_id to the Cooling wafer class following COO_PLT_manufacturer_batchid_waferid
        :return:
        """
        unique_id = base_models.lhcbID.objects.get(name = self._meta.label).cnt
        self.lhcb_id = 'COO_VCR_%d_%s_%s' % (unique_id,self.prod_id, self.batch_id)

    def __str__(self):
        """
        Print the details of this object
        :return: description string
        """
        return 'Cooling VCRs %s | manufacturer %s, prod id %s, type %s,grade %s, located in %s' % \
               (self.lhcb_id, self.manufacturer.name,self.prod_id,self.type,self.grade,self.location)



class BendCapillary(base_models.Process):
    """
    Bending of the capillary
    """
    class Meta:
        db_table = 'cooling_bend_capillary_processes'

    #: models.ForeignKey: Link to the capillary
    capillary =  models.ForeignKey(Capillary)

    def generate_process_name(self):
        self.name = '%s on %s'%(self.method.name,self.capillary.lhcb_id)


class BrazeConnector(base_models.Process):
    """
    Brazing of the cooling connectors with the capillaries and VCR

    Whether the capillaries have been brazed to the VCR before or after brazing to the connector, or all together
    is described in the method

    """
    class Meta:
        db_table = 'cooling_connector_brazing_processes'

    #: models.ForeignKey: Link to the left capillary
    capillary_left =  models.ForeignKey(Capillary,related_name='capillary_left_brazeconnectorcapillaries_set', null = True, blank = True)
    #: models.ForeignKey: Link to the right capillary
    capillary_right =  models.ForeignKey(Capillary,related_name='capillary_right_brazeconnectorcapillaries_set',null = True, blank = True)
    #: models.ForeignKey: Link to the left VCR
    vcr_left =  models.ForeignKey(Capillary,related_name='vcr_left_brazeconnectorcapillaries_set', null = True, blank = True)
    #: models.ForeignKey: Link to the right VCR
    vcr_right =  models.ForeignKey(Capillary,related_name='vcr_right_brazeconnectorcapillaries_set', null = True, blank = True)
    #: models.ForeignKey: Link to the connector
    connector =  models.ForeignKey(Connector)
    #: model.BooleanField : Generate ConnectorAssembly?
    automatic_assembly_creation = models.BooleanField(default=True)

    def save(self, *args, **kwargs):
        """
        Update the diced attribute of the wafer

        :param args:
        :param kwargs:
        :return:
        """

        super(BrazeConnector, self).save(*args, **kwargs)

        if self.automatic_assembly_creation:
            # Create the assembly
            self.connectorassembly_set.create(location=self.connector.location)

        self.automatic_assembly_creation = False

        super(BrazeConnector, self).save(*args, **kwargs)


    def generate_process_name(self):
        # TODO add the other items in the name, if not None
        self.name = '%s on %s'%(self.method.name,self.connector.lhcb_id)




class ConnectorAssembly(base_models.Item):
    """
    Silicon wafer for cooling
    """

    class Meta:
        db_table = 'cooling_connector_assembly_items'


    def generate_lhcb_id(self):
        """
        Assign a lhcb_id to the connector assembly class following COO_CAS_connector_lhcbid
        :return:
        """
        unique_id = base_models.lhcbID.objects.get(name = self._meta.label).cnt
        self.lhcb_id = 'COO_CAS_%d_%s' % (unique_id,self.connector.__str__()[8:])


    #: models.ForeignKey : Link to the brazing operation
    from_process = models.ForeignKey(BrazeConnector, on_delete=models.CASCADE)

    def __str__(self):
        """
        Print the details of this object
        :return: description string
        """
        return 'Cooling connector assembly %s | left capillary %s, right capillary %s,' \
               ' left VCR %s, right VCR %s, connector %s, grade %s, located in %s' % \
               (self.lhcb_id,
                self.from_process.capillary_left.lhcb_id,self.from_process.capillary_right.lhcb_id,
                self.from_process.vcr_left.lhcb_id,self.from_process.vcr_right.lhcb_id,
                self.from_process.connector.lhcb_id,self.grade,self.location)


class SolderingFoil(base_models.Item):
    """
    Silicon wafer for cooling
    """

    class Meta:
        db_table = 'cooling_soldering_foil_items'

    def generate_lhcb_id(self):
        """
        Assign a lhcb_id to the connector assembly class following COO_CAS_connector_lhcbid
        :return:
        """
        unique_id = base_models.lhcbID.objects.get(name = self._meta.label).cnt
        self.lhcb_id = 'COO_FOI_%d_%s' % (unique_id,self.batch_id)

    #: models.ForeignKey : Link to  to the manufacturer
    manufacturer = models.ForeignKey(CoolingManufacturer)
    #: models.CharField Manufacturer defined batch number
    batch_id = models.CharField(max_length=10)

    def __str__(self):
        """
        Print the details of this object
        :return: description string
        """
        return 'Cooling connector assembly %s | manufacturer %s, prod id %s, location %s'\
               %(self.lhcb_id, self.manufacturer.name, self.batch_id, self.grade, self.location)


class CutSolderingFoil(base_models.Process):
    """
    Cleaning of the soldering foil
    """
    class Meta:
        db_table = 'cooling_cut_soldering_foil_processes'

    #: models.ForeignKey : Type of object being metallised
    soldering_foil = models.ForeignKey(SolderingFoil, on_delete=models.CASCADE)

    def generate_process_name(self):
        self.name = '%s of %s'%(self.method.name,self.soldering_foil.lhcb_id)


class Clean(base_models.Process):
    """
    Cleaning of various items
    """
    class Meta:
        db_table = 'cooling_clean_processes'

    limit_choices_to = models.Q(app_label='cooling', model='solderingfoil')\
                       | models.Q(app_label='cooling', model='plate')\
                       | models.Q(app_label='cooling', model='connector')\
                       | models.Q(app_label='cooling', model='connectorassembly')
    #: models.ForeignKey : Type of object being cleaned
    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE, limit_choices_to=limit_choices_to)
    #: models.PositiveIntegerField : object identifier
    object_id = models.PositiveIntegerField( verbose_name = ('related object') )
    #: models.GenericForeignKey : Link to the cleaned object
    cleaned_object = GenericForeignKey('content_type', 'object_id')


    def generate_process_name(self):
        self.name = '%s on %s'%(self.method.name,self.cleaned_object.lhcb_id)



class MeltSolderingFoil(base_models.Process):
    """
    Cleaning of various items
    """
    class Meta:
        db_table = 'cooling_melt_soldering_foil_processes'

    limit_choices_to =  models.Q(app_label='cooling', model='plate')\
                       | models.Q(app_label='cooling', model='connector')\
                       | models.Q(app_label='cooling', model='connectorassembly')
    #: models.ForeignKey : Type of object being metallised
    melt_on_object_type = models.ForeignKey(ContentType, on_delete=models.CASCADE, limit_choices_to=limit_choices_to)
    #: models.PositiveIntegerField : object identifier
    object_id = models.PositiveIntegerField(verbose_name=('related object'))
    #: models.GenericForeignKey : Link to the cleaned object
    melt_on_object = GenericForeignKey('melt_on_object_type', 'object_id')
    #: models.ForeignKey : Link to the soldering foil
    soldering_foil = models.ForeignKey(SolderingFoil, on_delete=models.CASCADE)

    def generate_process_name(self):
        self.name = '%s %s on %s'%(self.method.name,self.soldering_foil.lhcb_id,self.melt_on_object.lhcb_id)




class Metallise(base_models.Process):
    """
    Metallisation of cooling connector or colling connector plus capillaries

    Whether it applies to the connector or the connector assembly is defined in the method
    """
    class Meta:
        db_table = 'cooling_metallise_processes'

    limit_choices_to = models.Q(app_label='cooling', model='connector')| models.Q(app_label='cooling', model='connectorassembly')
    #: models.ForeignKey : Type of object being metallised
    connector_assembly_type = models.ForeignKey(ContentType, on_delete=models.CASCADE, limit_choices_to=limit_choices_to)
    #: models.PositiveIntegerField : object identifier
    object_id = models.PositiveIntegerField(verbose_name=('related object'))
    #: models.GenericForeignKey : Link to the connector_assembly (Connector or ConnectorAssembly)
    connector_assembly = GenericForeignKey('connector_assembly_type', 'object_id')


    def generate_process_name(self):
        self.name = '%s on %s'%(self.method.name,self.connector_assembly.lhcb_id)


class RemoveMetallisation(base_models.Process):
    """
    Remove excedentary metallisation from cooling connector or colling connector plus capillaries

    Whether it applies to the connector or the connector assembly is defined in the method
    """

    class Meta:
        db_table = 'cooling_removemetallisation_processes'

    limit_choices_to = models.Q(app_label='cooling', model='connector') | models.Q(app_label='cooling',
                                                                                   model='connectorassembly')
    #: models.ForeignKey : Type of object whose metallisation is being removed (Connector or ConnectorAssembly)
    connector_assembly_type = models.ForeignKey(ContentType, on_delete=models.CASCADE,
                                                limit_choices_to=limit_choices_to)
    #: models.PositiveIntegerField : object identifier
    object_id = models.PositiveIntegerField(verbose_name=('related object'))
    #: models.GenericForeignKey : Link to the connector_assembly
    connector_assembly = GenericForeignKey('connector_assembly_type', 'object_id')

    def generate_process_name(self):
        self.name = '%s on %s'%(self.method.name,self.connector_assembly.lhcb_id)




### Qualifications

class AcousticMicroscope(base_models.Qualification):
    """
    Acoustic microscope qualification for the cooling wafers
    """
    class Meta:
        db_table = 'cooling_acoustic_microscope_qualifs'

    #: models.ImageField: image file of the acoustic microscope test
    microscope_result = models.ImageField(upload_to='cooling_acoustic_microscopes')
    #: models.ForeignKey: link to the tested wafer
    item_under_qualification = models.ForeignKey(Wafer, on_delete=models.CASCADE)

    def generate_qualification_name(self):
        self.name = '%s of %s'%(self.method.name,self.item_under_qualification.lhcb_id)



class PressureTest(base_models.Qualification):
    """
    Pressure test qualification for the cooling wafers pressure test structures
    """
    class Meta:
        db_table = 'cooling_pressure_test_qualifs'

    #: models.FloatField: Maximal pressure before break
    maximal_pressure = models.FloatField(help_text = "Maximal pressure before break",blank=True)
    #: models.FileField: Data file
    data = models.FileField(upload_to='cooling_wafer_pressure_test',blank=True)


    #: DB query : Object to which this qualification can be applied
    limit_choices_to = models.Q(app_label='cooling', model='pressureqc') \
                       | models.Q(app_label='cooling', model='connectorassembly')\
                       | models.Q(app_label='cooling', model='microchannelassembly')

    #: models.ForeignKey: Link to the type being tested
    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE,limit_choices_to=limit_choices_to,
                                     verbose_name="Type of object measured")
    #: models.PositiveIntegerField: identifier
    object_id = models.PositiveIntegerField(verbose_name="Object")
    #: models.ForeignKey: Link to the item being tested
    item_under_qualification = GenericForeignKey('content_type', 'object_id')


    def generate_qualification_name(self):
        self.name = '%s of %s'%(self.method.name,self.item_under_qualification.lhcb_id)



class VisualInspection(base_models.Qualification):
    """
    Visual inspection qualification
    """

    class Meta:
        db_table = 'cooling_visual_inspection'

    limit_choices_to = models.Q(app_label='cooling',name=None)
    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE,limit_choices_to=limit_choices_to)
    object_id = models.PositiveIntegerField()
    item_under_qualification = GenericForeignKey('content_type', 'object_id')

    def generate_qualification_name(self):
        self.name = '%s of %s'%(self.method.name,self.item_under_qualification.lhcb_id)


class OperatorReport(base_models.Qualification):
    """
    Operator report qualification
    """

    class Meta:
        db_table = 'cooling_operator_report'

    limit_choices_to = models.Q(app_label='cooling',lhcb_id=None)
    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE,limit_choices_to=limit_choices_to)
    object_id = models.PositiveIntegerField()
    process_under_qualification = GenericForeignKey('content_type', 'object_id')

    def generate_qualification_name(self):
        self.name = '%s of %s'%(self.method.name,self.process_under_qualification.name)



class Planarity(base_models.Qualification):
    """
    Pressure test qualification for the cooling wafers pressure test structures
    """
    class Meta:
        db_table = 'cooling_planarity_qualifs'

    #: models.FloatField: Maximal pressure before break
    spread = models.FloatField(help_text = "height spread",blank=True)
    #: models.FileField: Data file
    image = models.ImageField(upload_to='cooling_planarity',blank=True)

    #: DB query : Object to which this qualification can be applied
    limit_choices_to = models.Q(app_label='cooling', model='plate') \
                       | models.Q(app_label='cooling', model='connectorassembly')\
                       | models.Q(app_label='cooling', model='connector')\
                       | models.Q(app_label='cooling', model='microchannelassembly')

    #: models.ForeignKey: Link to the type being tested
    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE,limit_choices_to=limit_choices_to,
                                     verbose_name="Type of object measured")
    #: models.PositiveIntegerField: identifier
    object_id = models.PositiveIntegerField(verbose_name="Object")
    #: models.GenericForeignKey: Link to the item under qualification
    item_under_qualification = GenericForeignKey('content_type', 'object_id')

    def generate_qualification_name(self):
        self.name = '%s of %s'%(self.method.name,self.item_under_qualification.lhcb_id)

