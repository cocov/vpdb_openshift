from django.contrib import admin
from django.apps import apps
from django.utils.translation import ugettext as _, ugettext_lazy

import django.contrib.auth.admin

class VPDBAdminSite(admin.AdminSite):
    site_url = None

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._registry.update(admin.site._registry)

    def app_index(self, request, app_label, extra_context=None):
        app_name = apps.get_app_config(app_label).verbose_name
        if not extra_context:
            extra_context = {}
        title = _('{}'.format(app_name))
        extra_context.update({'title' : title})
        return super().app_index(request, app_label, extra_context)


vpdbsite = VPDBAdminSite(name='vpdb')
vpdbsite.site_url = None
vpdbsite.site_title = None
vpdbsite.index_title = 'Index'

admin.site.site_url = None
admin.site.site_title = None
admin.site.index_title = 'Index'

import generic.admin
import tiles.admin
import cooling.admin


