"""
This file was generated with the customdashboard management command and
contains the class for the main dashboard.

To activate your index dashboard add the following to your settings.py::
	GRAPPELLI_INDEX_DASHBOARD = 'vpdb.dashboard.CustomIndexDashboard'
"""

from django.utils.translation import ugettext_lazy as _
from django.core.urlresolvers import reverse

from grappelli.dashboard import modules, Dashboard
from grappelli.dashboard.utils import get_admin_site_name

import tiles.apps
import generic.apps
import cooling.apps


class CustomIndexDashboard(Dashboard):
	"""
	Custom index dashboard for VPDB Index.
	"""
	
	def init_with_context(self, context):
		site_name = get_admin_site_name(context)
		request = context['request']
		
		if request.user.is_superuser:
			self.children.append(modules.Group(
				_('Site Administration'),
				column=1,
				collapsible=True,
				css_classes=('grp-collapse','grp-closed'),
				children = [
					modules.LinkList(
						_(''),
						column=1,
						collapsible=False,
						children=[
							{
								'title': _('Administration (Users, Groups & Permissions)'),
								'url': '/vpdb/auth',
								'external': False,
							},
							{
								'title': _('Media Management'),
								'url': '/vpdb/filebrowser/browse/',
								'external': False,
							},
						]
					),
					modules.ModelList(
						_('Users, Groups & Permissions'),
						column=1,
						collapsible=True,
						css_classes=('grp-collapse','grp-closed'),
						models=('django.contrib.*',),
					),
				]
			))

                # add the applications
		self.children.append(generic.apps.index_group())
		self.children.append(tiles.apps.index_group())
		self.children.append(cooling.apps.index_group())

		# append a recent actions module
		if request.user.username != 'vpreader':
			self.children.append(modules.RecentActions(
				_('Your Recent Actions'),
				limit=5,
				collapsible=False,
				column=3,
			))


