"""
Django settings for contest project, production mode.

"""
# Get the standard development settings.
from vpdb.settings import *

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = False
TEMPLATE_DEBUG = False
ALLOWED_HOSTS = [u'*']
