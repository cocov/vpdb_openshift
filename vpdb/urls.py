"""vpdb URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
import filebrowser.sites
from django.conf import settings
from django.conf.urls import include
from django.conf.urls import url
from django.conf.urls.static import static
from django.views.defaults import page_not_found
from django.views.generic import RedirectView
from vpdb.admin import vpdbsite

urlpatterns = [
    url(r'^vpdb/generic/comment', page_not_found, {'exception': Exception('Not Found')}),
    url(r'^vpdb/filebrowser/', include(filebrowser.sites.site.urls)),
    url(r'^grappelli/', include('grappelli.urls')),
    url(r'^vpdb/', vpdbsite.urls, name='vpdb'),
    url(r'^$', RedirectView.as_view(url='vpdb/', permanent=True)),
]
