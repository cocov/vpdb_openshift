from vpdb.admin import vpdbsite as site

from base.admin import *
from .models import *

class TilesGroups:
    expert_group = 'tiles_expert'
    admin_group = 'tiles_admin'

class TilesModelAdmin(TilesGroups, VPDBModelAdmin):
    pass

class TilesStackedInline(TilesGroups, VPDBStackedInline):
    pass

class TilesTabularInline(TilesGroups, VPDBTabularInline):
    pass

class IVCurveTabularInline(TilesTabularInline):
    model = IVCurve
    fields = ('link','name', 'method', 'grade')
    fk_name = 'sensor'
    def get_readonly_fields(self, request, obj=None):
        readonly_fields = super().get_readonly_fields(request, obj)

        return readonly_fields

class SensorModelAdmin(TilesModelAdmin):
    inlines = (IVCurveTabularInline,)


class SensorTabularInline(TilesTabularInline):
    model = Sensor
    fields = ( 'link','lhcb_id', 'size', 'guard_ring_size' , 'implant_size','grade')
    #extra = 1
    fk_name = 'from_dicing'

    def get_readonly_fields(self, request, obj=None):
        readonly_fields = super().get_readonly_fields(request, obj)
        readonly_fields+=['lhcb_id']
        return readonly_fields

class DiceSensorWaferTabularInline(TilesTabularInline):
    model = DiceSensorWafer
    fields = ( 'link',)
    #extra = 1
    fk_name = 'wafer'
    inlines = [SensorTabularInline]
    def get_readonly_fields(self, request, obj=None):
        readonly_fields = super().get_readonly_fields(request, obj)

        return readonly_fields

class SensorWaferModelAdmin(TilesModelAdmin):
    inlines = ( DiceSensorWaferTabularInline, )


class VeloPixTabularInline(TilesTabularInline):
    model = VeloPix
    fields = ( 'link','lhcb_id', 'location')
    #extra = 1
    fk_name = 'from_dicing'

    def get_readonly_fields(self, request, obj=None):
        readonly_fields = super().get_readonly_fields(request, obj)
        readonly_fields+=['lhcb_id']
        return readonly_fields

class DiceVeloPixWaferTabularInline(TilesTabularInline):
    model = DiceVeloPixWafer
    fields = ( 'link',)
    #extra = 1
    fk_name = 'wafer'
    inlines = [VeloPixTabularInline]
    def get_readonly_fields(self, request, obj=None):
        readonly_fields = super().get_readonly_fields(request, obj)
        return readonly_fields

class VeloPixWaferModelAdmin(TilesModelAdmin):
    inlines = ( DiceVeloPixWaferTabularInline, )


site.register(TilesManufacturer, TilesModelAdmin)

site.register(SensorWafer, SensorWaferModelAdmin)
site.register(DiceSensorWafer, TilesModelAdmin)
site.register(Sensor, SensorModelAdmin)

site.register(VeloPixWafer, VeloPixWaferModelAdmin)
site.register(DiceVeloPixWafer, TilesModelAdmin)
site.register(VeloPix, TilesModelAdmin)
site.register(ThinVeloPixWafer, TilesModelAdmin)

site.register(BumpBond, TilesModelAdmin)
site.register(Tile, TilesModelAdmin)

site.register(VeloPixWaferProbeStationTest, TilesModelAdmin)
site.register(Equalisation, TilesModelAdmin)
site.register(SourceTest, TilesModelAdmin)
site.register(IVCurve, TilesModelAdmin)

