from django.apps import AppConfig
import base.apps

name = 'tiles'
verbose_name = 'Pixel Tiles'

class TilesConfig(AppConfig):
    name = name
    verbose_name = verbose_name

def index_group(): 
    return base.apps.app_group(name=name,
        title=verbose_name,
        title_suffix=' Project',
        direct_models='tiles.models.*')
