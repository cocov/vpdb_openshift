from django.db import models
from base import models as base_models
from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType


### Vocabulary
class TilesManufacturer(base_models.Manufacturer):
    """
    Manufacturer linked to the tiles project
    """
    class Meta:
        db_table = 'tiles_manufacturers'

    def __str__(self):
        return '%s %s %s' % (self.name, self.email, self.address)


### Items
class VeloPixWafer(base_models.Item):
    """
    VeloPix wafer
    """

    class Meta:
        db_table = 'tiles_velopix_wafers'

    VERSION_CHOICES = (
        ('VP1','VeloPix'),
        ('VP2','VeloPix 2')
    )
    #: models.ForeignKey : Link to the manufacturer
    manufacturer = models.ForeignKey(TilesManufacturer, help_text="VeloPix wafer manufacturer")
    #: models.IntegerField : Wafer ID
    wafer_id = models.IntegerField( help_text="Sensor wafer id in lot")
    #: models.IntegerField : Lot ID
    lot_id = models.IntegerField( help_text="Lot number")
    #: models.CharField : VeloPix wafer version
    version = models.CharField(max_length=8,choices=VERSION_CHOICES,default='VP2',help_text='VeloPix version')
    #: models.URLField : EDMS link to the design
    design_edms = models.URLField( blank=True )

    def generate_lhcb_id(self):
        """
        Assign a lhcb_id to the VeloPix wafer class following TIL_VPW_lotid_waferid_version
        :return:
        """
        unique_id = base_models.lhcbID.objects.get(name = self._meta.label).cnt
        self.lhcb_id = 'TIL_VPW_%d_%s_%s_%s'%(unique_id,self.lot_id,self.wafer_id,self.version)

    def save(self, *args, **kwargs):
        """
        Save the SensorWafer object:
        - Fill the EDMS document
        - Create the dicing process (as it is always done at the manufacturer)

        :param args:
        :param kwargs:
        :return:
        """

        # Assign the EDMS url as a function and generate the sensors
        if self.version=='VP1':
            self.design_edms = 'https://edms.cern.ch/document/1882822/1'
            pass
        elif self.version == 'VP2':
            self.design_edms = 'https://edms.cern.ch/document/1866187/1'

        super(VeloPixWafer, self).save(*args, **kwargs)




class SensorWafer(base_models.Item):
    """
    Sensor wafer
    """

    class Meta:
        db_table = 'tiles_sensor_wafers'

    SERIES_NUMBER_CHOICES = (
        ('S10938-3306', 'S10938-3306 VELO PIXEL SENSOR TYPE 2-1'),
        ('S10938-5202', 'S10938-5202 VELO PIXEL SENSOR TYPE 2-2'),
        ('S14419-6331','S14419-6331  VELO PIXEL SENSOR TYPE 2-2'),
    )



    #: models.ForeignKey : Link to the manufacturer
    manufacturer = models.ForeignKey(TilesManufacturer, help_text="Sensor wafer manufacturer")
    #: models.IntegerField : Wafer ID
    wafer_id = models.IntegerField( help_text="Sensor wafer id in lot")
    #: models.IntegerField : Lot ID
    lot_id = models.IntegerField( help_text="Lot number")
    #: models.CharField : Series number
    series_number = models.CharField(max_length=11,choices=SERIES_NUMBER_CHOICES,default = 'S14419-6331')
    #: models.URLField : EDMS link to the design
    design_edms = models.URLField( blank=True )

    def generate_lhcb_id(self):
        """
        Assign a lhcb_id to the Sensor wafer class following TIL_SPW_lotid_waferid_version
        :return:
        """
        unique_id = base_models.lhcbID.objects.get(name = self._meta.label).cnt
        self.lhcb_id = 'TIL_SEW_%d_%s_%s_%s'%(unique_id,self.lot_id,self.wafer_id,self.series_number)

    def save(self, *args, **kwargs):
        """
        Save the SensorWafer object:
        - Fill the EDMS document
        - Generate the sensors if it was required (self.automatic_sensor_creation)

        :param args:
        :param kwargs:
        :return:
        """

        # Assign the EDMS url as a function and generate the sensors
        if self.series_number in ['S10938-3306'] and not self.design_edms :
            self.design_edms = 'https://edms.cern.ch/document/1856152/1'

        elif self.series_number in ['S10938-5202', 'S14419-6331'] and not self.design_edms :
            self.design_edms = 'https://edms.cern.ch/document/1859520/1'

        super(SensorWafer, self).save(*args, **kwargs)



class DiceVeloPixWafer(base_models.Process):
    """
    Dicing of the velopix wafers
    """
    class Meta:
        db_table = 'tiles_dice_velopix_wafer_processes'

    #: models.ForeignKey : Link to the VeloPix wafer
    wafer = models.ForeignKey(VeloPixWafer, on_delete=models.CASCADE)
    #: models.BooleanField
    automatic_velopix_creation = models.BooleanField(default=True)
    #: models.BooleanField
    virtual = models.BooleanField(default=True)

    def save(self, *args, **kwargs):
        """
        Update the diced attribute of the wafer

        :param args:
        :param kwargs:
        :return:
        """

        super(DiceVeloPixWafer, self).save(*args, **kwargs)

        if self.automatic_velopix_creation:
            if self.version == 'VP2':
                for position in VeloPix.POSITION_CHOICES:
                    if position[0] in ['D1','I1']:continue
                    self.velopix_set.create(position=position[0], location=self.wafer.location)
            if self.version == 'VP1':
                for position in VeloPix.POSITION_CHOICES:
                    if position[0] in ['A7','L7','B8','C9','D9','I9','J9','E10','F10','G10','H10']:continue
                    self.velopix_set.create(position=position[0], location=self.wafer.location)


        self.automatic_velopix_creation = False

        if self.end_date != None :
            self.virtual = False

        super(DiceVeloPixWafer, self).save(*args, **kwargs)


class ThinVeloPixWafer(base_models.Process):
    """
    Thining of the velopix wafers
    """

    class Meta:
        db_table = 'tiles_thin_velopix_wafer_processes'
    #: models.ForeignKey : Link to the VeloPix Wafer being thinned
    wafer = models.ForeignKey(VeloPixWafer, on_delete=models.CASCADE)

    def save(self, *args, **kwargs):
        """
        Update the diced attribute of the wafer

        :param args:
        :param kwargs:
        :return:
        """
        if not self.wafer.thinned :
            self.wafer.thinned = True

        super(ThinVeloPixWafer, self).save(*args, **kwargs)


class VeloPix(base_models.Item):
    """
    VeloPix wafer
    """
    class Meta:
        db_table = 'tiles_velopixs'

    def generate_lhcb_id(self):
        """
        Generate an LHCb id following the sequence TIL_VPX_WaferID_LotID_Position
        :return:
        """
        unique_id = base_models.lhcbID.objects.get(name = self._meta.label).cnt
        self.lhcb_id = 'TIL_VPX_%d_W%s-%s_%s'%(unique_id,self.from_dicing.wafer.lot_id,self.from_dicing.wafer.wafer_id,self.position)

    POSITION_CHOICES = (
        ('D1', 'D1'),('E1','E1'),('F1','F1'),('G1','G1'),('H1','H1'),('I1','I1'),
        ('C2', 'C2'),('D2','D2'),('E2','E2'),('F2','F2'),('G2','G2'),('H2','H2'),('I2','I2'),('J2','J2'),('K2','K2'),
        ('B3', 'B3'),('C3','C3'),('D3','D3'), ('E3','E3'), ('F3','F3'), ('G3','G3'), ('H3','H3'), ('I3','I3'), ('J3','J3'), ('K3','K3'), ('L3','L3'),
        ('A4', 'A4'),('B4', 'B4'),('C4','C4'),('D4','D4'), ('E4','E4'), ('F4','F4'), ('G4','G4'), ('H4','H4'), ('I4','I4'), ('J4','J4'), ('K4','K4'),('L4','L4'),
        ('A5', 'A5'),('B5', 'B5'),('C5','C5'),('D5','D5'), ('E5','E5'), ('F5','F5'), ('G5','G5'), ('H5','H5'), ('I5','I5'), ('J5','J5'), ('K5','K5'),('L5','L5'),
        ('A6', 'A6'),('B6', 'B6'),('C6','C6'),('D6','D6'), ('E6','E6'), ('F6','F6'), ('G6','G6'), ('H6','H6'), ('I6','I6'), ('J6','J6'), ('K6','K6'),('L6','L6'),
        ('A7', 'A7'),('B7', 'B7'),('C7','C7'),('D7','D7'), ('E7','E7'), ('F7','F7'), ('G7','G7'), ('H7','H7'), ('I7','I7'), ('J7','J7'), ('K7','K7'),('L7','L7'),
        ('B8', 'B8'),('C8','C8'),('D8','D8'), ('E8','E8'), ('F8','F8'), ('G8','G8'), ('H8','H8'), ('I8','I8'), ('J8','J8'),
        ('C9','C9'),('D9','D9'), ('E9','E9'), ('F9','F9'), ('G9','G9'), ('H9','H9'), ('I9','I9'), ('J9','J9'),
        ('E10','E10'), ('F10','F10'), ('G10','G10'), ('H10','H10'),
    )
    #: models.CharField : Position in the wafer (possible choices in POSITION_CHOICES)
    position = models.CharField(max_length=3 , choices=POSITION_CHOICES )
    #: models.IntegerField : Burned ID, filled whenever the chip ID is burned (ie. read-only on the view)
    burned_id = models.IntegerField(null=True,blank=True)
    #: models.ForeignKey: Link to the Dice VeloPix Wafer process
    from_dicing = models.ForeignKey(DiceVeloPixWafer, on_delete=models.CASCADE,help_text="The wafer dicing process that generated it")

    def save(self, *args, **kwargs):
        """
        Check if the position is compatible with the wafer version and save

        :param args:
        :param kwargs:
        :return:
        """
        if self.from_dicing.wafer.version == 'VP2' and self.position in ['D1', 'I1']:
            raise Exception('%s position does not exist in VeloPix2 wafers'%self.position)

        if self.from_dicing.wafer.version == 'VP1' and self.position in ['A7', 'L7', 'B8', 'C9',
                                                                         'D9', 'I9', 'J9', 'E10',
                                                                         'F10', 'G10', 'H10']:

            raise Exception('%s position does not exist in VeloPix1 wafers'%self.position)

        super(VeloPix, self).save(*args, **kwargs)



class DiceSensorWafer(base_models.Process):
    """
    Dicing of the cooling wafers
    """
    class Meta:
        db_table = 'tiles_dice_sensor_wafer_processes'

    wafer = models.ForeignKey(SensorWafer, on_delete=models.CASCADE)
    #: models.BooleanField :
    automatic_sensor_creation = models.BooleanField( default=True )

    def generate_process_name(self):
        self.name = '%s on %s'%(self.method.name,self.wafer.lhcb_id)

    def save(self, *args, **kwargs):
        """
        Update the diced attribute of the wafer

        :param args:
        :param kwargs:
        :return:
        """

        super(DiceSensorWafer, self).save(*args, **kwargs)

        if self.automatic_sensor_creation :
            # Generate the sensors
            if self.wafer.series_number in ['S10938-3306']:
                for p in range(10):
                    self.sensor_set.create(position='P%d' % p, size='T', implant_size=35, guard_ring_size=450, location = self.wafer.location)
                for p in range(4):
                    self.sensor_set.create(position='P%d' % p, size='T', implant_size=35, guard_ring_size=600, location = self.wafer.location)
                for p in range(4):
                    self.sensor_set.create(position='P%d' % p, size='S', implant_size=35, guard_ring_size=450, location = self.wafer.location)
                for p in range(2):
                    self.sensor_set.create(position='P%d' % p, size='S', implant_size=39, guard_ring_size=450, location = self.wafer.location)
                for p in range(2):
                    self.sensor_set.create(position='P%d' % p, size='S', implant_size=35, guard_ring_size=600, location = self.wafer.location)
                for p in range(2):
                    self.sensor_set.create(position='P%d' % p, size='S', implant_size=39, guard_ring_size=600, location = self.wafer.location)

            elif self.wafer.series_number in ['S10938-5202', 'S14419-6331']:
                for p in range(16):
                    self.sensor_set.create(position='P%d' % p, size='T', implant_size=39, guard_ring_size=450, location = self.wafer.location)
                self.sensor_set.create(position='S', size='S', implant_size=39, guard_ring_size=450, location = self.wafer.location)
                self.sensor_set.create(position='S', size='S', implant_size=39, guard_ring_size=600, location = self.wafer.location)
                self.sensor_set.create(position='S', size='S', implant_size=35, guard_ring_size=450, location = self.wafer.location)

        self.automatic_sensor_creation = False

        super(DiceSensorWafer, self).save(*args, **kwargs)




class Sensor(base_models.Item):
    """
    VeloPix wafer
    """
    class Meta:
        db_table = 'tiles_sensors'
    SIZE_CHOICES = (
        ('S', 'S'),
        ('T', 'T')
    )
    POSITION_CHOICES = (
        ('P1','P1'),
        ('P2','P2'),
        ('P3','P3'),
        ('P4','P4'),
        ('P5','P5'),
        ('P6','P6'),
        ('P7','P7'),
        ('P8','P8'),
        ('P9','P9'),
        ('P10','P10'),
        ('P11','P11'),
        ('P12','P12'),
        ('P13','P13'),
        ('P14','P14'),
        ('P15','P15'),
        ('P16','P16'),
        ('S','S'),
    )

    def generate_lhcb_id(self):
        """
        Generate an LHCb id following the sequence TIL_SEN_PrimaryKey_WaferID_SensorID
        :return:
        """
        unique_id = base_models.lhcbID.objects.get(name = self._meta.label).cnt
        self.lhcb_id = 'TIL_SEN_%d_W%s-%s_%s'%(unique_id,self.from_dicing.wafer.lot_id,self.from_dicing.wafer.wafer_id,self.producer_id)

    def save(self, *args, **kwargs):
        if not self.producer_id:
            if self.size == 'T':
                self.producer_id = '%s%d-%d-%s'%(
                    self.size,self.implant_size,self.guard_ring_size,self.position)
            else:
                self.producer_id = '%s%d-%d' % (
                    self.size, self.implant_size, self.guard_ring_size)
        print('saved sensor',self.producer_id)
        super(Sensor, self).save(*args, **kwargs)

    producer_id = models.CharField(max_length=40,
                                help_text="Producer identifier (automatic)",null = True,blank = True)
    position = models.CharField(max_length=3, choices=POSITION_CHOICES,
                                help_text="Sensor position on the wafer (only for triplets)",blank = True)
    size = models.CharField(max_length=1, choices=SIZE_CHOICES, default='T')
    implant_size = models.IntegerField(help_text="Size of the implants (in um)",default=39)
    guard_ring_size = models.IntegerField(help_text="Size of the guard ring (in um)",default = 450)

    from_dicing = models.ForeignKey(DiceSensorWafer, on_delete=models.CASCADE,
                                     help_text="The wafer dicing process that generated it")


class BumpBond(base_models.Process):
    """
    Thinning of the cooling wafers
    """
    class Meta:
        db_table = 'tiles_bump_bond_processes'

    sensor = models.ForeignKey(Sensor, on_delete=models.CASCADE)
    velopix_0 = models.ForeignKey(VeloPix, on_delete=models.CASCADE, related_name='velopix_0_bumpbonding_set')
    velopix_1 = models.ForeignKey(VeloPix, on_delete=models.CASCADE, blank=True, related_name='velopix_1_bumpbonding_set')
    velopix_2 = models.ForeignKey(VeloPix, on_delete=models.CASCADE, blank=True, related_name='velopix_2_bumpbonding_set') # check if blank if it is a singlet


class Tile(base_models.Item):
    """
    VeloPix wafer
    """
    def generate_lhcb_id(self):
        """
        Generate an LHCb id following the sequence TIL_SEN_PrimaryKey_WaferID_SensorID
        :return:
        """
        unique_id = base_models.lhcbID.objects.get(name = self._meta.label).cnt
        self.lhcb_id = 'TIL_%d'%(unique_id)

    class Meta:
        db_table = 'tiles_tiles'

    from_process = models.ForeignKey(BumpBond, on_delete=models.CASCADE)


class VeloPixWaferProbeStationTest(base_models.Qualification):

    class Meta:
        db_table = 'tiles_velopix_wafer_probestation_qualif'

    pass


class IVCurve(base_models.Qualification):
    class Meta:
        db_table = 'tiles_ivcurve_qualif'

    data_file = models.FileField(upload_to='iv_curves/')
    sensor = models.ForeignKey(Sensor, on_delete=models.CASCADE)
    temperature = models.FloatField(help_text="Temperature at which the IV curve was taken [C]")
    vaccum = models.FloatField(help_text="Pressure [bar]")

    def generate_qualification_name(self):
        self.name = '%s on sensor %s'%(self.method.name,self.sensor.lhcb_id)


class SourceTest(base_models.Qualification):
    class Meta:
        db_table = 'tiles_source_test_qualif'

    pass


class Equalisation(base_models.Qualification):
    class Meta:
        db_table = 'tiles_equalisation_qualif'

    pass


